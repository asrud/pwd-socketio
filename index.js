const http = require("http");
const express = require("express");
const cors = require("cors");
const app = express();
const server = http.createServer(app);
const socketIo = require("socket.io");

const PORT = process.env.PORT || 8000;

// socket config
const io = socketIo(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
    allowedHeaders: ["Access-Control-Allow-Origin"],
    Credential: true,
  },
});
// const io = socketIo(server);
app.use(cors());
app.use(express.json());

app.io = io;
let arrMsg = [];
let arrChn = [];
let usersRoom1 = [];
let arrRoom = [];
app.arrMsg = arrMsg;

app.post("/sendMessage", (req, res) => {
  if (req.query.namespace == "default") {
    if (req.body.room) {
      // middle ware room
      arrRoom.push(req.body);
      io.in(req.body.room).emit("messagesRoom", arrRoom);
    } else {
      // global room
      arrMsg.push(req.body);
      io.emit("chat message", arrMsg);
    }
    res.status(200).send("send meesage success");
  } else if (req.query.namespace == "channel") {
    arrChn.push(req.body);
    channelNsp.emit("chat message", arrChn);
    res.status(200).send(arrChn);
  }
});

io.on("connection", (socket) => {
  socket.on("JoinChat", (data) => {
    console.log("User join :", data);
  });

  socket.on("JoinRoom", (data) => {
    socket.join(data.room);
    usersRoom1.push({ ...data, id: socket.id });
    io.in(data.room).emit("notifRoom1", `${data.nama} just entered the room`);
  });

  socket.on("disconnection", () => {
    console.log("user disconnection");
  });
});

const channelNsp = io.of("/channel");
channelNsp.on("connection", (socket) => {
  socket.on("JoinChat", (data) => {
    console.log("user join channel: ", data);
  });

  socket.on("disconnect", () => {
    console.log("user disconnect from channel");
  });
});

server.listen(PORT, () => console.log("socket server is running"));
